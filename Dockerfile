## NOTE: to retain configuration; mount a Docker volume, or use a bind-mount, on /var/lib/zerotier-one
FROM debian:buster-slim as builder

## Supports x86_64, x86, arm, and arm64
ENV DEBIAN_FRONTEND=noninteractive
RUN apt-get update && apt-get install -y curl gnupg
RUN apt-key adv --keyserver ha.pool.sks-keyservers.net --recv-keys 0x1657198823e52a61  && \
    echo "deb http://download.zerotier.com/debian/buster buster main" > /etc/apt/sources.list.d/zerotier.list
RUN apt-get update && apt-get install -y zerotier-one

FROM debian:buster-slim
LABEL description="Containerized ZeroTier One for use on CoreOS or other Docker-only Linux hosts."

# ZeroTier relies on UDP port 9993
EXPOSE 9993/udp
ENTRYPOINT ["/main.sh"]
CMD ["zerotier-one"]
ENV DEBIAN_FRONTEND=noninteractive

RUN apt update && apt-get -y install supervisor iputils-ping nmap balance procps && apt-get clean && rm -rf /var/cache/apt

RUN mkdir -p /var/lib/zerotier-one
COPY --from=builder /usr/sbin/zerotier-cli /usr/sbin/zerotier-cli
COPY --from=builder /usr/sbin/zerotier-idtool /usr/sbin/zerotier-idtool
COPY --from=builder /usr/sbin/zerotier-one /usr/sbin/zerotier-one
COPY main.sh balance.sh /
COPY supervisord.conf /etc/supervisor/supervisord.conf
