#!/bin/bash

# Local server, needed to bind only on Docker interface
LSERVER=$(grep $(hostname) /etc/hosts |cut -f 1)

# Wait for networking
while :
do
    (echo >/dev/tcp/${RSERVER}/${RPORT}) &>/dev/null && break
    sleep 1
done

exec balance -f -b ::ffff:$LSERVER $LPORT ${RSERVER}:${RPORT}
