#!/bin/bash

# Please configure RSERVER, RPORT and LPORT variables in a .env file before starting the container
source .env

docker run -t --name zerotier -d --env-file=$(pwd)/.env --rm -p${LPORT}:${LPORT} --device=/dev/net/tun --cap-add=NET_ADMIN -v $(pwd)/conf:/var/lib/zerotier-one registry.gitlab.com/ppizzo/zerotier
