# ZeroTier Docker image

Docker image for [ZeroTier](https://www.zerotier.com/).

The image comes with basic network tools to be used for network diagnostics, they can be removed if not needed.

The image also provides [balance](https://balance.inlab.net/) software: you can route connections coming from a local port to a service running on a remote host.

Before starting the image for the first time you need to create a `.env` file in current directory with the following content:

```sh
RSERVER=xxx.xxx.xxx.xxx
RPORT=80
LPORT=8080
```

where:

* `RSERVER` is the IP address of remote server
* `RPORT` is the port on remote server to connect to
* `LPORT` is the local port for routing connection to remote server

Adapt the variables above to your environment.

After starting the image please exec `zerotier-cli join ...` in the running container to join your network.

## References

* https://github.com/zerotier/ZeroTierOne/tree/master/ext/installfiles/linux/zerotier-containerized
